Commerce Checkage
--------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides a commerce checkout pane with an
'I confirm I am over 18' checkbox.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_checkage

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_checkage

REQUIREMENTS
------------

This module requires the following modules:

 * [Commerce](https://www.drupal.org/project/commerce)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Enable the module as usual.
 * Go to Commerce > Configuration > Orders > Checkout Flows.
 * Edit a checkout flow to place and configure the "Commerce Checkage" pane.

MAINTAINERS
-----------

Current maintainers:
 * Alex Burrows https://www.drupal.org/u/aburrows
 * Pascal Crott https://www.drupal.org/u/hydra
