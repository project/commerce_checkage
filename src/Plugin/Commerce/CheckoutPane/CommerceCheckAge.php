<?php

namespace Drupal\commerce_checkage\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;

/**
 * @CommerceCheckoutPane(
 *  id = "commerce_check_age",
 *  label = @Translation("Commerce check age"),
 *  admin_label = @Translation("Commerce check age"),
 * )
 */
class CommerceCheckAge extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['age_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I confirm I am over 18'),
      '#required' => TRUE
    ];

    return $pane_form;
  }

}
